# Easy Docker Config
This aims to simplify docker configurations for project basis.

## Getting Started
Please follow below steps to setup docker for any project

- Add the docker-compose.yml and .env.sample files to the project root (Same level as codepool).
- Rename .env.sample to .env
- Update .env configurations
- Then run 

    ```
    docker-compose up
    ```
    
### Environment configuration guide
 available configuration options in .env file.
```shell
    ## PROJECT
    COMPOSE_PROJECT_NAME=example
    IP_FIRST_OCTET=10
    HOST_NAME=example.ns-staging.com.au
    
    ## MYSQL
    MYSQL_VERSION=5.7
    MYSQL_DB_NAME=database_name
    MYSQL_DB_USER=database_user
    MYSQL_DB_PASS=database_pass
    
    ## MAGENTO
    MAGE_MODE=developer
```
- **COMPOSE_PROJECT_NAME** - This will be used as project prefix. It'll be helpful you to undentify containers separately when you run multiple docker containers simultaneously.
- **IP_FIRST_OCTET** - You can customize this to have a number ranged from 1-255.
    - Service IPs
        - web service - XX.0.0.10
        - mysql service - XX.0.0.20
        - varnish service - XX.0.0.30
        - elasticsearch service - XX.0.0.40
        - redis service - XX.0.0.50
    - Network IPs
        - subnet - XX.0.0.0/24
        - gateway - XX.0.0.1
- **HOST_NAME** - hostname to set. ex: example.ns-staging.com.au
- **MYSQL_VERSION** - MySQL version to use
- **MYSQL_DB_NAME** - Database name to create database in docker container
- **MYSQL_DB_USER** - Database user password to create in docker container
- **MYSQL_DB_PASS** - Database user password to set in docker container
- **MAGE_MODE** - Magento run mode

## Prerequisites
- Project directory shoud structured as netstarter standards
- User must installed
    - [docker](https://docs.docker.com/install/ "docker")
    - [docker-compose](https://docs.docker.com/compose/install/ "docker-compose")
- Must have setup netstarter docker resgistry properly

## Related Articles
Please follow below articles written by Netstartians.

- [Install and configure docker by Suresh](https://confluence.sd.salmat.com.au/confluence/display/NLC/Docker "Install and configure docker")
- [How to use docker compose by Suresh](https://confluence.sd.salmat.com.au/confluence/display/NLC/Docker+Compose "How to use docker compose")
- [Docker stack setup guide by Dasitha](https://confluence.sd.salmat.com.au/confluence/pages/viewpage.action?pageId=105873543 "Docker stack setup guide")